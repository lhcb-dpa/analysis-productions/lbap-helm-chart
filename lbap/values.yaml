# Default values for LHCb Productions Submission infrastructure
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.
image:
  repository: gitlab-registry.cern.ch/lhcb-dpa/analysis-productions/lbap-helm-chart
  tag: 7ab03e7e
  pullPolicy: Always

nameOverride: ""
fullnameOverride: ""

serviceAccount:
  # Specifies whether a service account should be created
  create: true
  # Annotations to add to the service account
  annotations: {}
  # The name of the service account to use.
  # If not set and create is true, a name is generated using the fullname template
  name: ""

podAnnotations: {}

podSecurityContext: {}

securityContext: {}

developer:
  enabled: true
  # Path from which to mount the sources
  lbapSourcesPath: /sources/
  lbapInstallationPath: /opt/conda/lib/python3.12/site-packages
  ciLocalScratchPath: /sources/.demo/jobs
  userProxyDir: /sources/.demo/credentials

lbap:
  diracCfgPath: /cvmfs/lhcb.cern.ch/lhcbdirac/etc/dirac.cfg
  settings:
    LBAP_DB_URL: mysql://root:mysqlRootDevInsecure@lbap-demo-mysql:3306/my_database
    LBAP_LBAPI_BACKEND: mysql://root:mysqlRootDevInsecure@lbap-demo-mysql:3306/celeryresults
    LBAP_OPENSEARCH: https+noverify://admin:admin@opensearch-cluster-master:9200
    LBAP_TOKEN_SALT: insecure-token-salt
    LBAP_LBAPI_URL: "http://anything.invalid:8000"
    LBAP_S3: '{"endpoint": "http://anything.invalid:32000", "bucket": "lbaptestdata", "access_key_id": "console", "secret_access_key": "console123"}'

lbapi:
  enabled: true
  replicaCount: 1
  strategy:
    type: "RollingUpdate"
  resources:
    requests:
      memory: "400Mi"
      cpu: "100m"
    limits:
      memory: "700Mi"
      cpu: "1000m"

celery:
  enabled: true
  replicaCount: 1
  strategy:
    type: "Recreate"
  resources:
    requests:
      memory: "500Mi"
      cpu: "100m"
    limits:
      memory: "950Mi"
      cpu: "1000m"

beat:
  enabled: false
  replicaCount: 1
  strategy:
    type: "Recreate"
  resources:
    requests:
      memory: "180Mi"
      cpu: "1m"
    limits:
      memory: "250Mi"
      cpu: "100m"

lbapweb:
  image: gitlab-registry.cern.ch/lhcb-dpa/analysis-productions/lbapweb:latest
  strategy:
    type: "RollingUpdate"
  resources:
    requests:
      memory: "20Mi"
      cpu: "1m"
    limits:
      memory: "50Mi"
      cpu: "100m"

minio:
  enabled: true
  service:
    type: NodePort
  consoleService:
    type: NodePort
  ingress:
    enabled: false
  consoleIngress:
    enabled: false
  resources:
    requests:
      memory: 512Mi
  replicas: 1
  persistence:
    enabled: false
  mode: standalone
  rootUser: rootuser
  rootPassword: rootpass123
  environment:
    MINIO_BROWSER_REDIRECT_URL: http://anything.invalid:32001/
  buckets:
    - name: lbaptestdata
      policy: none
  users:
    - accessKey: console
      secretKey: console123
      policy: consoleAdmin

opensearch:
  enabled: true
  opensearchJavaOpts: "-Xms256m -Xmx256m"
  # replicas: 1
  singleNode: true
  config:
    cluster.routing.allocation.disk.threshold_enabled: "true"
    cluster.routing.allocation.disk.watermark.flood_stage: 200mb
    cluster.routing.allocation.disk.watermark.low: 500mb
    cluster.routing.allocation.disk.watermark.high: 300mb
    plugins.security.disabled: "true"
  # extraEnvs:
  #   - name: DISABLE_INSTALL_DEMO_CONFIG
  #     value: "true"

mysql:
  enabled: true
  auth:
    rootPassword: mysqlRootDevInsecure
    password: mysqlDevInsecure
  initdbScripts:
    my_init_script.sh: |
      #!/bin/sh
      mysql -P 3306 -uroot -pmysqlRootDevInsecure -e "create database celeryresults";

rabbitmq:
  enabled: true
  podSecurityContext:
    enabled: false
  containerSecurityContext:
    enabled: false
  extraConfiguration: |-
    consumer_timeout = 36000000

cvmfs:
  enabled: true
  repositories:
    - lhcb.cern.ch
    - cernvm-prod.cern.ch
    - lhcb-condb.cern.ch

global:
  tag: 5.1.19
  http:
    enabled: false

eos:
  enabled: false
  fst:
    replicaCount: 1
    persistence:
      enabled: false
      size: 10GiB
    extraEnv:
      EOS_FST_ALIAS: anything.invalid
      EOS_FST_PORT_ALIAS: "32101"
  mgm:
    replicaCount: 1

service:
  type: ClusterIP
  port: 8000

ingress:
  enabled: true

resources: {}
  # We usually recommend not to specify default resources and to leave this as a conscious
  # choice for the user. This also increases chances charts run on environments with little
  # resources, such as Minikube. If you do want to specify resources, uncomment the following
  # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
  # limits:
  #   cpu: 100m
  #   memory: 128Mi
  # requests:
  #   cpu: 100m
  #   memory: 128Mi

autoscaling:
  enabled: false
  minReplicas: 1
  maxReplicas: 1
  targetCPUUtilizationPercentage: 80
  # targetMemoryUtilizationPercentage: 80

nodeSelector: {}

tolerations: []

affinity: {}
