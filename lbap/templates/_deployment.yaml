{{- define "lbap.deployment_base" }}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "lbap.fullname" . }}-{{ .Template.Name | base | trimSuffix ".yaml" }}
  labels:
    {{- include "lbap.labels" . | nindent 4 }}
spec:
  {{- if not .Values.autoscaling.enabled }}
  replicas: {{ .replicaCount }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "lbap.selectorLabels" . | nindent 6 }}
      app: {{ .Template.Name | base | trimSuffix ".yaml" }}
  strategy:
    {{- toYaml .strategy | nindent 4 }}
  template:
    metadata:
      annotations:
        {{- with .Values.podAnnotations }}
        {{- toYaml . | nindent 8 }}
        {{- end }}
        checksum/settings: {{ toYaml .Values.lbap.settings | sha256sum }}
        checksum/config: {{ include (print $.Template.BasePath "/configmap.yaml") . | sha256sum }}
      labels:
        {{- include "lbap.selectorLabels" . | nindent 8 }}
        app: {{ .Template.Name | base | trimSuffix ".yaml" }}
    spec:
      {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      serviceAccountName: {{ include "lbap.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      volumes:
        - name: cs-store-mount
          {{- if and (.Values.developer.enabled) (.Values.developer.localCSPath) }}
          persistentVolumeClaim:
            claimName: pvc-cs-store
          {{- else }}
          emptyDir:
          {{- end }}
        - name: cvmfs-mount
          persistentVolumeClaim:
            claimName: pvc-cvmfs
        {{- if .Values.developer.enabled }}
        - name: lbap-code-mount
          persistentVolumeClaim:
            claimName: pvc-lbap-code
        - name: lbap-proxy-mount
          persistentVolumeClaim:
            claimName: pvc-user-proxy
        - name: lbap-ci-local-scratch-mount
          persistentVolumeClaim:
            claimName: pvc-ci-local-scratch
        {{- end }}
        - name: signing-key-mount
          emptyDir:
        {{- if .Values.eos.enabled }}
        - name: eos-sss-keytab
          secret:
            secretName: eos-sss-keytab
            defaultMode: 0400
        - name: eos-sss-keytab-fixedownership
          emptyDir: {}
        {{- end }}
        - name: config
          configMap:
            name: logging-config
      initContainers:
        {{- if .Values.mysql.enabled }}
        - name: wait-for-mysql
          image: busybox:latest
          imagePullPolicy: IfNotPresent
          command: ['sh', '-c', 'until nc -vz {{ .Release.Name }}-mysql 3306; do echo "Waiting for mysql..."; sleep 3; done;']
        {{- end }}
        {{- if .Values.opensearch.enabled }}
        - name: wait-for-opensearch
          image: busybox:latest
          imagePullPolicy: IfNotPresent
          command: ['sh', '-c', 'until nc -vz  opensearch-cluster-master 9200; do echo "Waiting for opensearch..."; sleep 3; done;']
        {{- end }}
        {{- if .Values.eos.enabled }}
        # Required until https://github.com/kubernetes/kubernetes/issues/81089 is merged
        - name: eos-sss-keytab-ownership
          image: alpine:latest
          command: ["/bin/sh", "-c"]
          args: ["cp /root/sss_keytab/input/eos.keytab /root/sss_keytab/output/eos.keytab; chown 1000:1000 /root/sss_keytab/output/eos.keytab; chmod 400 /root/sss_keytab/output/eos.keytab"]
          volumeMounts:
            - name: eos-sss-keytab
              mountPath: /root/sss_keytab/input/eos.keytab
              subPath: eos.keytab
            - name: eos-sss-keytab-fixedownership
              mountPath: /root/sss_keytab/output
          {{- end }}
      containers:
        - name: {{ $.Chart.Name }}
          resources:
            {{- toYaml .resources | nindent 12 }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default $.Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          ports:
            {{- toYaml (default .ports dict) | nindent 12 }}
          livenessProbe:
            {{- toYaml (default .livenessProbe dict) | nindent 12 }}
          readinessProbe:
            {{- toYaml (default .readinessProbe dict) | nindent 12 }}
          args:
            {{- toYaml .containerArgs | nindent 12 }}
          volumeMounts:
            - mountPath: /cs_store
              name: cs-store-mount
              readOnly: true
            - mountPath: /signing-key/
              name: signing-key-mount
              readOnly: true
            - mountPath: /cvmfs/
              name: cvmfs-mount
              readOnly: true
              {{- if not .Values.cvmfs.enabled }}
              mountPropagation: HostToContainer
              {{- end }}
            {{- if .Values.developer.enabled }}
            - mountPath: /credentials
              name: lbap-proxy-mount
              readOnly: true
            {{- end }}
            {{- if .Values.developer.enabled }}
            - mountPath: {{ .Values.developer.lbapInstallationPath }}/LbAPI
              subPath: LbAPI/src/LbAPI
              name: lbap-code-mount
              readOnly: true
            - mountPath: {{ .Values.developer.lbapInstallationPath }}/LbAnalysisProductions
              subPath: LbAnalysisProductions/src/LbAnalysisProductions
              name: lbap-code-mount
              readOnly: true
            - mountPath: {{ .Values.developer.lbapInstallationPath }}/LbAPCommon
              subPath: LbAPCommon/src/LbAPCommon
              name: lbap-code-mount
              readOnly: true
            - mountPath: {{ .Values.lbap.settings.LBAP_CI_JOB_LOCAL_DIR }}
              name: lbap-ci-local-scratch-mount
            {{- end }}
            {{- if .Values.eos.enabled }}
            - name: eos-sss-keytab-fixedownership
              mountPath: /etc/eos.keytab
              subPath: eos.keytab
            {{- end }}
            - name: config
              mountPath: "/config"
              readOnly: true
          env:
            {{- if .Values.eos.enabled }}
            - name: EOS_MGM_URL
              value: root://lbap-demo-mgm-0.lbap-demo-mgm.default.svc.cluster.local
            {{- end }}
            {{- if .Values.developer.enabled }}
            - name: X509_USER_PROXY
              value: /credentials/proxy
            {{- end }}
            - name: DIRACSYSCONFIG
              value: {{ .Values.lbap.diracCfgPath }}
            - name: LBAP_LOGGING_CONFIG_INI
              value: /config/log-conf.ini
            - name: DEBUG_ALLOW_ALL_BK_PATHS
              value: x
          envFrom:
          - secretRef:
              name: lbap-secrets
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
{{- end }}
