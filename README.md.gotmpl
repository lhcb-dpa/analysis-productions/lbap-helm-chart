# Helm chart for LHCb Production Request Infrastructure

{{ template "chart.description" . }}

{{ template "chart.versionBadge" . }}{{ template "chart.typeBadge" . }}{{ template "chart.appVersionBadge" . }}

## Running locally

To run a copy of the LHCb production submission infrastructure locally you must first get a LHCb grid proxy locally.
Once you have this file you can start the demo with:

```bash
git clone --recurse-submodules ssh://git@gitlab.cern.ch:7999/lhcb-dpa/analysis-productions/lbap-helm-chart.git
cd lbap-helm-chart/

docker run --rm -it -v $PWD/LbAPWeb:/code -w /code jupyter/minimal-notebook:latest ./download_dependencies.sh
sed -i.bak 's@https://lbap.app.cern.ch@http://localhost:8000@g' LbAPWeb/components/common.jsx

./run_demo.sh /path/to/grid/proxy
```

After a few minutes you should be able to access the dashboard using the displayed URLs.

### Caveats

* This demo runs against the production instance of LHCbDIRAC
* Normal user proxies are unable to perform many actions used by LbAPI
* This demo downloads ~10GB of data while starting
* The celery worker does not automatically reload when the code changes and the pod must be deleted to trigger
* GitLab is accessed in read-only mode

### Interacting with the demo

Various scripts are available in `scripts/` for easing interaction

#### Kubernetes basics

Assuming you have exported the environment variables printed by the demo script you can interact with the demo cluster using:

```bash
# List the running pods
kubectl get pods
# Get some more information about a pod
kubectl describe pod/<pod name>
# Show the logs of a running pod
kubectl logs <pod name>
# Show the logs of a running pod and keep following them
kubectl logs -f <pod name>
# Run a command in one of the non-LbAPI pods
kubectl exec -it <pod name> -- /bin/bash
# Run a command in one of the LbAPI pods with the conda environment loaded
kubectl exec -it <pod name> -- /dockerMicroMambaEntrypoint.sh bash
```

#### Helm basics

#### Registering the test repository

To get an API token which can be later used for sending webhooks:

```bash
scripts/create-test-repo-webhook.sh MC_REQUEST
```

You can also replace `MC_REQUEST` with `ANA_PROD`.

#### Triggering the webhook

Using the token from above, a JSON payload can be sent using:

```bash
scripts/trigger-test-repo-webhook.sh lbapi:webhook:rmmC0v75iEyqtM0d_zqFrwt5PPrM6zzG4EQXLB9FGkQ scripts/data/mc-requests/example-webhook.json
```

You should now see jobs start appearing in the web interface.

#### Executing a test job properly

To run a test job with the results being streamed back to your local machine:

```bash
scripts/run-local-test-job-over-ssh.sh LOCAL_JOB_ID username@lxplus.cern.ch
```

You will be prompted the password for connecting to the remote host twice as well as for your grid certificate password.


## Deploying in production

TODO: Set this up with GitLab CI.

{{ template "chart.requirementsSection" . }}

{{ template "chart.valuesSection" . }}

{{ template "helm-docs.versionFooter" . }}
