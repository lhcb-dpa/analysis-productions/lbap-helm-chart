# Helm chart for LHCb Production Request Infrastructure

This helm chart is intended to be used in two ways:

 * Development: The ./run_demo.sh script allows the infrastructure to be ran locally with docker+kind
 * Production: Deploying to CERN's PaaS platform

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 0.0.1a](https://img.shields.io/badge/AppVersion-0.0.1a-informational?style=flat-square)

## Running locally

To run a copy of the LHCb production submission infrastructure locally you must first get a LHCb grid proxy locally.
Once you have this file you can start the demo with:

```bash
git clone --recurse-submodules ssh://git@gitlab.cern.ch:7999/lhcb-dpa/analysis-productions/lbap-helm-chart.git
cd lbap-helm-chart/

docker run --rm -it -v $PWD/LbAPWeb:/code -w /code jupyter/minimal-notebook:latest ./download_dependencies.sh
sed -i.bak 's@https://lbap.app.cern.ch@http://localhost:8000@g' LbAPWeb/components/common.jsx

./run_demo.sh /path/to/grid/proxy
```

After a few minutes you should be able to access the dashboard using the displayed URLs.

### Caveats

* This demo runs against the production instance of LHCbDIRAC
* Normal user proxies are unable to perform many actions used by LbAPI
* This demo downloads ~10GB of data while starting
* The celery worker does not automatically reload when the code changes and the pod must be deleted to trigger
* GitLab is accessed in read-only mode

### Interacting with the demo

Various scripts are available in `scripts/` for easing interaction

#### Kubernetes basics

Assuming you have exported the environment variables printed by the demo script you can interact with the demo cluster using:

```bash
# List the running pods
kubectl get pods
# Get some more information about a pod
kubectl describe pod/<pod name>
# Show the logs of a running pod
kubectl logs <pod name>
# Show the logs of a running pod and keep following them
kubectl logs -f <pod name>
# Run a command in one of the non-LbAPI pods
kubectl exec -it <pod name> -- /bin/bash
# Run a command in one of the LbAPI pods with the conda environment loaded
kubectl exec -it <pod name> -- /dockerMicroMambaEntrypoint.sh bash
```

#### Helm basics

#### Registering the test repository

To get an API token which can be later used for sending webhooks:

```bash
scripts/create-test-repo-webhook.sh MC_REQUEST
```

You can also replace `MC_REQUEST` with `ANA_PROD`.

#### Triggering the webhook

Using the token from above, a JSON payload can be sent using:

```bash
scripts/trigger-test-repo-webhook.sh lbapi:webhook:rmmC0v75iEyqtM0d_zqFrwt5PPrM6zzG4EQXLB9FGkQ scripts/data/mc-requests/example-webhook.json
```

You should now see jobs start appearing in the web interface.

#### Executing a test job properly

To run a test job with the results being streamed back to your local machine:

```bash
scripts/run-local-test-job-over-ssh.sh LOCAL_JOB_ID username@lxplus.cern.ch
```

You will be prompted the password for connecting to the remote host twice as well as for your grid certificate password.

## Deploying in production

TODO: Set this up with GitLab CI.

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://charts.bitnami.com/bitnami/ | mysql | 9.11.0 |
| https://charts.bitnami.com/bitnami/ | rabbitmq | 12.0.10 |
| https://charts.min.io/ | minio | 5.0.11 |
| https://opensearch-project.github.io/helm-charts/ | opensearch | 2.13.1 |
| https://registry.cern.ch/chartrepo/eos | eos(server) | 0.1.7 |
| https://registry.cern.ch/chartrepo/sciencebox | cvmfs | 0.0.7 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` |  |
| autoscaling.enabled | bool | `false` |  |
| autoscaling.maxReplicas | int | `1` |  |
| autoscaling.minReplicas | int | `1` |  |
| autoscaling.targetCPUUtilizationPercentage | int | `80` |  |
| beat.enabled | bool | `false` |  |
| beat.replicaCount | int | `1` |  |
| beat.resources.limits.cpu | string | `"100m"` |  |
| beat.resources.limits.memory | string | `"250Mi"` |  |
| beat.resources.requests.cpu | string | `"1m"` |  |
| beat.resources.requests.memory | string | `"180Mi"` |  |
| beat.strategy.type | string | `"Recreate"` |  |
| celery.enabled | bool | `true` |  |
| celery.replicaCount | int | `1` |  |
| celery.resources.limits.cpu | string | `"1000m"` |  |
| celery.resources.limits.memory | string | `"950Mi"` |  |
| celery.resources.requests.cpu | string | `"100m"` |  |
| celery.resources.requests.memory | string | `"500Mi"` |  |
| celery.strategy.type | string | `"Recreate"` |  |
| cvmfs.enabled | bool | `true` |  |
| cvmfs.repositories[0] | string | `"lhcb.cern.ch"` |  |
| cvmfs.repositories[1] | string | `"cernvm-prod.cern.ch"` |  |
| cvmfs.repositories[2] | string | `"lhcb-condb.cern.ch"` |  |
| developer.ciLocalScratchPath | string | `"/sources/.demo/jobs"` |  |
| developer.enabled | bool | `true` |  |
| developer.lbapInstallationPath | string | `"/opt/conda/lib/python3.12/site-packages"` |  |
| developer.lbapSourcesPath | string | `"/sources/"` |  |
| developer.userProxyDir | string | `"/sources/.demo/credentials"` |  |
| eos.enabled | bool | `false` |  |
| eos.fst.extraEnv.EOS_FST_ALIAS | string | `"anything.invalid"` |  |
| eos.fst.extraEnv.EOS_FST_PORT_ALIAS | string | `"32101"` |  |
| eos.fst.persistence.enabled | bool | `false` |  |
| eos.fst.persistence.size | string | `"10GiB"` |  |
| eos.fst.replicaCount | int | `1` |  |
| eos.mgm.replicaCount | int | `1` |  |
| fullnameOverride | string | `""` |  |
| global.http.enabled | bool | `false` |  |
| global.tag | string | `"5.1.19"` |  |
| image.pullPolicy | string | `"Always"` |  |
| image.repository | string | `"gitlab-registry.cern.ch/lhcb-dpa/analysis-productions/lbap-helm-chart"` |  |
| image.tag | string | `"7ab03e7e"` |  |
| ingress.enabled | bool | `true` |  |
| lbap.diracCfgPath | string | `"/cvmfs/lhcb.cern.ch/lhcbdirac/etc/dirac.cfg"` |  |
| lbap.settings.LBAP_DB_URL | string | `"mysql://root:mysqlRootDevInsecure@lbap-demo-mysql:3306/my_database"` |  |
| lbap.settings.LBAP_LBAPI_BACKEND | string | `"mysql://root:mysqlRootDevInsecure@lbap-demo-mysql:3306/celeryresults"` |  |
| lbap.settings.LBAP_LBAPI_URL | string | `"http://anything.invalid:8000"` |  |
| lbap.settings.LBAP_OPENSEARCH | string | `"https+noverify://admin:admin@opensearch-cluster-master:9200"` |  |
| lbap.settings.LBAP_S3 | string | `"{\"endpoint\": \"http://anything.invalid:32000\", \"bucket\": \"lbaptestdata\", \"access_key_id\": \"console\", \"secret_access_key\": \"console123\"}"` |  |
| lbap.settings.LBAP_TOKEN_SALT | string | `"insecure-token-salt"` |  |
| lbapi.enabled | bool | `true` |  |
| lbapi.replicaCount | int | `1` |  |
| lbapi.resources.limits.cpu | string | `"1000m"` |  |
| lbapi.resources.limits.memory | string | `"700Mi"` |  |
| lbapi.resources.requests.cpu | string | `"100m"` |  |
| lbapi.resources.requests.memory | string | `"400Mi"` |  |
| lbapi.strategy.type | string | `"RollingUpdate"` |  |
| lbapweb.image | string | `"gitlab-registry.cern.ch/lhcb-dpa/analysis-productions/lbapweb:latest"` |  |
| lbapweb.resources.limits.cpu | string | `"100m"` |  |
| lbapweb.resources.limits.memory | string | `"50Mi"` |  |
| lbapweb.resources.requests.cpu | string | `"1m"` |  |
| lbapweb.resources.requests.memory | string | `"20Mi"` |  |
| lbapweb.strategy.type | string | `"RollingUpdate"` |  |
| minio.buckets[0].name | string | `"lbaptestdata"` |  |
| minio.buckets[0].policy | string | `"none"` |  |
| minio.consoleIngress.enabled | bool | `false` |  |
| minio.consoleService.type | string | `"NodePort"` |  |
| minio.enabled | bool | `true` |  |
| minio.environment.MINIO_BROWSER_REDIRECT_URL | string | `"http://anything.invalid:32001/"` |  |
| minio.ingress.enabled | bool | `false` |  |
| minio.mode | string | `"standalone"` |  |
| minio.persistence.enabled | bool | `false` |  |
| minio.replicas | int | `1` |  |
| minio.resources.requests.memory | string | `"512Mi"` |  |
| minio.rootPassword | string | `"rootpass123"` |  |
| minio.rootUser | string | `"rootuser"` |  |
| minio.service.type | string | `"NodePort"` |  |
| minio.users[0].accessKey | string | `"console"` |  |
| minio.users[0].policy | string | `"consoleAdmin"` |  |
| minio.users[0].secretKey | string | `"console123"` |  |
| mysql.auth.password | string | `"mysqlDevInsecure"` |  |
| mysql.auth.rootPassword | string | `"mysqlRootDevInsecure"` |  |
| mysql.enabled | bool | `true` |  |
| mysql.initdbScripts."my_init_script.sh" | string | `"#!/bin/sh\nmysql -P 3306 -uroot -pmysqlRootDevInsecure -e \"create database celeryresults\";\n"` |  |
| nameOverride | string | `""` |  |
| nodeSelector | object | `{}` |  |
| opensearch.config."cluster.routing.allocation.disk.threshold_enabled" | string | `"true"` |  |
| opensearch.config."cluster.routing.allocation.disk.watermark.flood_stage" | string | `"200mb"` |  |
| opensearch.config."cluster.routing.allocation.disk.watermark.high" | string | `"300mb"` |  |
| opensearch.config."cluster.routing.allocation.disk.watermark.low" | string | `"500mb"` |  |
| opensearch.config."plugins.security.disabled" | string | `"true"` |  |
| opensearch.enabled | bool | `true` |  |
| opensearch.opensearchJavaOpts | string | `"-Xms256m -Xmx256m"` |  |
| opensearch.singleNode | bool | `true` |  |
| podAnnotations | object | `{}` |  |
| podSecurityContext | object | `{}` |  |
| rabbitmq.containerSecurityContext.enabled | bool | `false` |  |
| rabbitmq.enabled | bool | `true` |  |
| rabbitmq.extraConfiguration | string | `"consumer_timeout = 36000000"` |  |
| rabbitmq.podSecurityContext.enabled | bool | `false` |  |
| resources | object | `{}` |  |
| securityContext | object | `{}` |  |
| service.port | int | `8000` |  |
| service.type | string | `"ClusterIP"` |  |
| serviceAccount.annotations | object | `{}` |  |
| serviceAccount.create | bool | `true` |  |
| serviceAccount.name | string | `""` |  |
| tolerations | list | `[]` |  |

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.13.1](https://github.com/norwoodj/helm-docs/releases/v1.13.1)
