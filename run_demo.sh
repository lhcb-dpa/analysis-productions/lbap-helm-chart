#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

UNICORN_EMOJI="\U1F984"
ERROR_EMOJI="\U1F6A8"
SKULL_EMOJI="\U1F480"

function cleanup(){
  trap - SIGTERM;
  echo "Cleaning up";
  if [[ -f "${demo_dir}/kind" ]] && [[ -f "${KUBECONFIG}" ]]; then
      "${demo_dir}/kind" delete cluster --name lbap-demo
  fi
  rm -rf "${tmp_dir}"
}

function check_hostname(){
  # Check that the hostname resolves to an IP address
  # dig doesn't consider the effect of /etc/hosts so we use ping instead
  ip_address=$(ping -c 1 "$1" | grep -Eo '([0-9]{1,3}\.){3}[0-9]{1,3}' | head -n 1)
  if [[ -z "${ip_address}" ]]; then
    printf "%b No IP address found hostname %s\n" ${SKULL_EMOJI} "${1}"
    return 1
  fi
  if [[ "${ip_address}" == 127.* ]]; then
    printf "%b Hostname %s resolves to 127.0.0.1 but this is not supported\n" ${SKULL_EMOJI} "${1}"
    return 1
  fi
}

machine_hostname=$(hostname | tr '[:upper:]' '[:lower:]')
if ! check_hostname "${machine_hostname}"; then
  machine_hostname=$(ifconfig | grep 'inet ' | awk '{ print $2 }' | grep -v '^127' | head -n 1 | cut -d '/' -f 1)
  if ! check_hostname "${machine_hostname}"; then
    echo "Failed to find an appropriate hostname for the demo."
    exit 1
  fi
  printf "%b Using IP address %s instead \n" ${ERROR_EMOJI} "${machine_hostname}"
fi

script_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

demo_dir="${script_dir}/.demo"
mkdir -p "${demo_dir}"
mkdir -p "${demo_dir}/jobs"
mkdir -p "${demo_dir}/credentials"
export KUBECONFIG="${demo_dir}/kube.conf"
export HELM_DATA_HOME="${demo_dir}/helm_data"

if [[ ! -f "${1:-}" ]]; then
  printf "%b Usage: run_demo.sh <path to proxy file>\n" ${ERROR_EMOJI}
  exit 1
fi
cp "${1}" "${demo_dir}/credentials/proxy"

tmp_dir=$(mktemp -d)

trap "cleanup" EXIT

if [[ ! -f "${demo_dir}/helm" ]]; then
  # Inspect the current system
  system_name=$(uname -s | tr '[:upper:]' '[:lower:]')
  system_arch=$(uname -m)
  if [[ "${system_arch}" == "x86_64" ]]; then
      system_arch="amd64"
  fi

  # Download kind
  printf "%b Downloading kind\n" ${UNICORN_EMOJI}
  curl --no-progress-meter -L "https://kind.sigs.k8s.io/dl/v0.19.0/kind-${system_name}-${system_arch}" > "${demo_dir}/kind"

  # Download kubectl
  printf "%b Downloading kubectl\n" ${UNICORN_EMOJI}
  latest_version=$(curl -L -s https://dl.k8s.io/release/stable.txt)
  curl --no-progress-meter -L "https://dl.k8s.io/release/${latest_version}/bin/${system_name}/${system_arch}/kubectl" > "${demo_dir}/kubectl"

  # Download helm
  printf "%b Downloading helm\n" ${UNICORN_EMOJI}
  curl --no-progress-meter -L "https://get.helm.sh/helm-v3.12.0-${system_name}-${system_arch}.tar.gz" > "${tmp_dir}/helm.tar.gz"
  mkdir -p "${tmp_dir}/helm-tarball"
  tar xzf "${tmp_dir}/helm.tar.gz" -C "${tmp_dir}/helm-tarball"
  mv "${tmp_dir}/helm-tarball/${system_name}-${system_arch}/helm" "${demo_dir}"

  # Make the binaries executable
  chmod +x "${demo_dir}/kubectl" "${demo_dir}/kind" "${demo_dir}/helm"

  # Install helm plugins to ${HELM_DATA_HOME}
  "${demo_dir}/helm" plugin install https://github.com/databus23/helm-diff
fi

printf "%b Generating Kind cluster template...\n" ${UNICORN_EMOJI}
sed "s@{{ script_dir }}@${script_dir}@g" "${script_dir}/demo/demo_cluster_conf.tpl.yaml" > "${demo_dir}/demo_cluster_conf.yaml"
if grep '{{' "${demo_dir}/demo_cluster_conf.yaml"; then
  printf "%b Error generating Kind template. Found {{ in the template result\n" ${ERROR_EMOJI}
  exit 1
fi

printf "%b Starting Kind cluster...\n" ${UNICORN_EMOJI}
"${demo_dir}/kind" create cluster \
  --kubeconfig "${KUBECONFIG}" \
  --wait "1m" \
  --config "${demo_dir}/demo_cluster_conf.yaml" \
  --name lbap-demo

printf "%b Creating an ingress...\n" ${UNICORN_EMOJI}
"${demo_dir}/kubectl" apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml
printf "%b Waiting for ingress controller to be created...\n" ${UNICORN_EMOJI}
"${demo_dir}/kubectl" wait --namespace ingress-nginx \
  --for=condition=ready pod \
  --selector=app.kubernetes.io/component=controller \
  --timeout=90s

printf "%b Generating Helm templates\n" ${UNICORN_EMOJI}
sed "s/{{ hostname }}/${machine_hostname}/g" "${script_dir}/demo/values.tpl.yaml" > "${demo_dir}/values.yaml"
if grep '{{' "${demo_dir}/values.yaml"; then
  printf "%b Error generating template. Found {{ in the template result\n" ${ERROR_EMOJI}
  exit 1
fi

printf "%b Installing LbAPI...\n" ${UNICORN_EMOJI}
"${demo_dir}/helm" install lbap-demo "${script_dir}/lbap" --values "${demo_dir}/values.yaml" #--post-renderer "${script_dir}/kustomize/kustomize"
printf "%b Waiting for installation to finish...\n" ${UNICORN_EMOJI}
if "${demo_dir}/kubectl" wait --for=condition=ready pod --selector app.kubernetes.io/instance=lbap-demo --timeout=900s; then
  # printf "%b Configuring EOS\n" ${UNICORN_EMOJI}
  # kubectl exec -it lbap-demo-mgm-0 -- sh -c 'eos space config default space.token.generation=1'
  # kubectl exec -it lbap-demo-mgm-0 -- sh -c 'eos vid publicaccesslevel 0'

  printf "\U1F389 \U1F389 \U1F389 Pods are ready! \U1F389 \U1F389 \U1F389\n"
else
  printf "%b Installation did not start sucessfully!\n" ${ERROR_EMOJI}
  printf "%b EOS will not have been configured sucessfully!!!\n" ${ERROR_EMOJI}
fi

echo ""
printf "\U2139\UFE0F  To interact with the cluster:\n"
echo "export KUBECONFIG=${KUBECONFIG}"
echo "export HELM_DATA_HOME=${HELM_DATA_HOME}"
echo "export PATH=\${PATH}:${demo_dir}"
echo ""
printf "\U2139\UFE0F  You can now access services at:\n"
printf "      LbAPI: http://localhost:8000/docs\n"
printf "      minio: http://%s:32001/\n" "${machine_hostname}"
printf "      LbAPWeb: http://localhost:3000\n"
echo ""
printf "\U2139\UFE0F  See README.md for more ways to interact with the demo.\n"
echo ""
printf "\U2139\UFE0F  The LbAPI/LbAnalysisProductions/LbAPCommon/LbAPWeb directories in ${script_dir} are mounted within the pods and will be automatically reloaded when changes are made.\n"
echo ""
printf "\U2139\UFE0F  Press Ctrl+C to clean up and exit\n"

while true; do
  sleep 60;
  if ! check_hostname "${machine_hostname}"; then
    echo "The demo will likely need to be restarted."
  fi
done
