#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

script_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

webhook_type=$1
if [[ "${webhook_type}" != "MC_REQUEST" ]] && [[ "${webhook_type}" != "ANA_PROD"  ]]; then
    echo "Unrecognised webhook type: ${webhook_type}"
    exit 1
fi

pod_name=$("${script_dir}/../.demo/kubectl" get pods --selector=app=lbapi -o json | jq -r '.items[] | .metadata.name' | head -n 1)

exec "${script_dir}/../.demo/kubectl" exec -it "${pod_name}" -- /dockerMicroMambaEntrypoint.sh ipython -c 'from LbAPI.ci import actions, queries
from LbAPI.enums import WebhookType
from LbAPI.database import create_engine
await create_engine()
await queries.create_webhook(148168, "lhcb-dpa/analysis-productions/test-repo", WebhookType.'${webhook_type}')'
