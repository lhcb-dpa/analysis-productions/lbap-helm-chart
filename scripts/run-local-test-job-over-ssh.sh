#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

UNICORN_EMOJI="\U1F984"
ERROR_EMOJI="\U1F6A8"

local_job_id=$1
ssh_hostname=$2

script_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
local_job_workdir="${script_dir}/../.demo/jobs/lbapi-${local_job_id}/"

if [[ ! -d "${local_job_workdir}" ]]; then
    echo "Available job IDs are:"
    echo "$(for fn in "${script_dir}"/../.demo/jobs/lbapi-*/; do echo "$(basename "${fn}")" | cut -d '-' -f 2-; done)"
    printf "%b '%s' does not appear to be a valid job id\n" ${ERROR_EMOJI} ${local_job_id}
    printf "%b Job workdir %s does not exist\n" ${ERROR_EMOJI} ${local_job_workdir}
    exit 1
fi

rsync_dest="${ssh_hostname}:/tmp/lbapi-${local_job_id}/"
printf "%b Copying files to %s \n" "${UNICORN_EMOJI}" "${rsync_dest}"
rsync --archive --recursive --progress "${local_job_workdir}/" "${rsync_dest}"

printf "%b Running test job on %s\n" "${UNICORN_EMOJI}" "${ssh_hostname}"
machine_hostname=$(hostname | tr '[:upper:]' '[:lower:]')
set -x
ssh -t -R 8000:localhost:8000 -R 32100:localhost:32100 -R 32101:localhost:32101 ${ssh_hostname} bash -xc \
    "\"/cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/stable/linux-64/bin/apptainer exec --hostname '${machine_hostname}' --bind /cvmfs --userns /cvmfs/cernvm-prod.cern.ch/cvm4 bash -c 'source /cvmfs/lhcb.cern.ch/lib/LbEnv; { lhcb-proxy-info --checkvalid || lhcb-proxy-init; } && cd /tmp/lbapi-${local_job_id}/ && lb-dirac env LBAP_NO_UPLOAD_OUTPUT_DATA=x python job_script.py'\""
