#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

script_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

pod_name=$("${script_dir}/../.demo/kubectl" get pods --selector=app=celery-worker -o json | jq -r '.items[] | .metadata.name' | head -n 1)
echo "Picked pod $pod_name"

exec "${script_dir}/../.demo/kubectl" exec -it "${pod_name}" -- /dockerMicroMambaEntrypoint.sh "$@"
