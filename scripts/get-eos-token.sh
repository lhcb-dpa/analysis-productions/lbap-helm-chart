#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

script_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

pod_name=$("${script_dir}/../.demo/kubectl" get pods --selector=app=lbapi -o json | jq -r '.items[] | .metadata.name' | head -n 1)
exec "${script_dir}/../.demo/kubectl" exec -it "${pod_name}" -- /dockerMicroMambaEntrypoint.sh python -c 'from datetime import datetime, timedelta; import asyncio; from LbAPI.utils import generate_eos_token; print(asyncio.run(generate_eos_token("/eos", True, "daemon", datetime.now() + timedelta(days=7))))'
