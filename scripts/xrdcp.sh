#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

machine_hostname=$(hostname | tr '[:upper:]' '[:lower:]')

a=$2
b=$3
if [[ "$a" = /eos/* ]]; then
    a="root://${machine_hostname}:32100/$a?authz=$1"
fi
if [[ "$b" = /eos/* ]]; then
    b="root://${machine_hostname}:32100/$b?authz=$1"
fi

exec xrdcp $a $b
