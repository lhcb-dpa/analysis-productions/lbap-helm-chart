#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

webhook_token=$1
payload_fn=$2

machine_hostname=$(hostname | tr '[:upper:]' '[:lower:]')

exec curl -X "POST" \
  "http://${machine_hostname}:8000/ci/webhook" \
  -H "accept: application/json" \
  -H "x-gitlab-token: ${webhook_token}" \
  -H "x-gitlab-event: Merge Request Hook" \
  -d "@${payload_fn}"
