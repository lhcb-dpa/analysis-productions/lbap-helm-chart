#!/usr/bin/env bash

eval "$(micromamba shell hook --shell=posix)" && micromamba activate base

echo "Running $@"
exec "$@"
