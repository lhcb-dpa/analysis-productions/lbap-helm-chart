#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

celery -A LbAPI.celery.app inspect registered

echo "Entering debug environment"
echo "Example commands:"
echo ""
echo "LbAPI.celery.start_pipeline(project_id: int, commit_sha: str, mr_id: int)"
echo "LbAPI.celery.start_cirun(pipeline_id: int, cirun_name: str)"
echo "LbAPI.celery.launch_production("ANA_PROD", NNNN)"
echo ""

exec /dockerMicroMambaEntrypoint.sh python -i -c 'import LbAPI.celery; LbAPI.celery.on_setup_logging(); LbAPI.celery.setup_globals(); from LbAPI.celery import loop'
