# syntax=docker/dockerfile:1-labs
FROM gitlab-registry.cern.ch/linuxsupport/cs9-base:latest
ARG LBAPI_REF
ARG LBANALYISPRODUCTIONS_REF

# libquadmath is a hack!
RUN yum install -y epel-release libquadmath && \
    echo $'[eos9-stable]\nname=EOS binaries from CERN Koji [stable]\nbaseurl=https://storage-ci.web.cern.ch/storage-ci/eos/diopside/tag/testing/el-9/x86_64\nenabled=1\ngpgcheck=False\ngpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-koji file:///etc/pki/rpm-gpg/RPM-GPG-KEY-kojiv2\npriority=9' > /etc/yum.repos.d/eos9-stable.repo && \
    echo $'[eos9-stable-depend]\nname=EOS binaries dependencies from CERN Koji [stable]\nbaseurl=https://storage-ci.web.cern.ch/storage-ci/eos/diopside-depend/el-9/x86_64\nenabled=1\ngpgcheck=False\ngpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-koji file:///etc/pki/rpm-gpg/RPM-GPG-KEY-kojiv2\npriority=9' > /etc/yum.repos.d/eos9-stable-depend.repo && \
    dnf install -y eos-client bzip2 && \
    yum clean all && \
    rm -rf /var/cache/yum

ENV MAMBA_ROOT_PREFIX=/opt/conda \
    PATH=/opt/conda/bin:$PATH \
    EOS_MGM_URL=root://eoslhcb.cern.ch \
    MAMBA_USER=1000

RUN mkdir -p /opt/conda && chown -R $MAMBA_USER /opt/conda

USER $MAMBA_USER

RUN cd /opt/conda && \
    curl -Ls https://micro.mamba.pm/api/micromamba/linux-64/latest | tar -xvj bin/micromamba && \
    eval "$(micromamba shell hook -s posix)"

# Create empty directory for the various volume to be mounted
# and assign them to mambauser
# This is needed for the DIRAC integration test, as docker-compose
# isn't playing well with permissions and volumes
USER root
RUN mkdir /signing-key && chown $MAMBA_USER:$MAMBA_USER /signing-key
USER $MAMBA_USER

WORKDIR /code

COPY --chown=$MAMBA_USER:$MAMBA_USER LbAPI/environment.yaml /code/
# openssh is needed for ssh-keygen when we generate signing key
RUN micromamba install --yes --file environment.yaml --name=base git openssh &&  micromamba clean --all --yes

ADD --chown=$MAMBA_USER:$MAMBA_USER --keep-git-dir=true https://gitlab.cern.ch/lhcb-dpa/analysis-productions/LbAPI.git#$LBAPI_REF /code/LbAPI
RUN pip install /code/LbAPI/

# Copying in ENTRYPOINT script
COPY --chown=$MAMBA_USER:$MAMBA_USER dockerEntrypoint.sh /
RUN chmod 755 /dockerEntrypoint.sh

# Copying the mamba specific entrypoint with lower ulimit
COPY --chown=$MAMBA_USER:$MAMBA_USER dockerMicroMambaEntrypoint.sh /
RUN chmod 755 /dockerMicroMambaEntrypoint.sh

# Add debug tools
COPY --chown=$MAMBA_USER:$MAMBA_USER debug.sh /
RUN chmod 755 /debug.sh

ENTRYPOINT [ "/dockerEntrypoint.sh" ]
